package com.afs.tdd;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TurnRightCommandTest {

    public static Stream<Arguments> testTurnRightData() {
        return Stream.of(
                Arguments.of(0, 0, Direction.North, 0, 0, Direction.East),
                Arguments.of(1, 1, Direction.South, 1, 1, Direction.West),
                Arguments.of(0, 0, Direction.East, 0, 0, Direction.South),
                Arguments.of(0, 0, Direction.West, 0, 0, Direction.North)
        );
    }

    public static Stream<Arguments> testUndoData() {
        return Stream.of(
                Arguments.of(0, 0, Direction.North, 0, 0, Direction.North),
                Arguments.of(1, 1, Direction.South, 1, 1, Direction.South),
                Arguments.of(0, 0, Direction.East, 0, 0, Direction.East),
                Arguments.of(0, 0, Direction.West, 0, 0, Direction.West)
        );
    }

    @ParameterizedTest()
    @MethodSource("testTurnRightData")
    public void should_change_correct_xy_when_executeCommand_given_xy_and_turnRight(int x, int y, Direction direction,
                                                                               int expectedX, int expectedY, Direction expectedDirection) {
        Location location = new Location(x, y, direction);
        MarsRover marsRover = new MarsRover(location);
        Command  turnRightCommand= new TurnRightCommand(marsRover);
        turnRightCommand.execute();

        assertEquals(expectedX, location.getX());
        assertEquals(expectedY, location.getY());
        assertEquals(expectedDirection, location.getDirection());
    }

    @ParameterizedTest()
    @MethodSource("testUndoData")
    public void should_constant_xy_and_direction_when_executeCommand_given_xy_and_turnRight_and_undo(int x, int y, Direction direction,
                                                                               int expectedX, int expectedY, Direction expectedDirection) {
        Location location = new Location(x, y, direction);
        MarsRover marsRover = new MarsRover(location);
        Command  turnRightCommand= new TurnRightCommand(marsRover);
        turnRightCommand.execute();
        turnRightCommand.undo();

        assertEquals(expectedX, location.getX());
        assertEquals(expectedY, location.getY());
        assertEquals(expectedDirection, location.getDirection());
    }
}
