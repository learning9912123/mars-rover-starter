//package com.afs.tdd;
//
//import org.junit.jupiter.api.Test;
//
//import java.util.Arrays;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//public class MulCommandTest {
//    @Test
//    public void should_change_correct_xy_and_direction_when_executeCommand_given_xy_and_direction_and_command(){
//        Location location = new Location(0, 0, Direction.North);
//        MarsRover marsRover = new MarsRover(location);
//
//        List<Command> commands = Arrays.asList(Command.Move, Command.TurnLeft, Command.Move, Command.TurnRight);
//        marsRover.executeMulCommand(commands);
//
//        assertEquals(-1, location.getX());
//        assertEquals(1, location.getY());
//        assertEquals(Direction.North, location.getDirection());
//    }
//}
