package com.afs.tdd;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TurnLeftCommandTest {

    public static Stream<Arguments> testTurnLeftData() {
        return Stream.of(
                Arguments.of(0, 0, Direction.North, 0, 0,Direction.West),
                Arguments.of(1, 1, Direction.South, 1, 1,Direction.East),
                Arguments.of(0, 0, Direction.East, 0, 0,Direction.North),
                Arguments.of(0, 0, Direction.West, 0, 0,Direction.South)
        );
    }

    public static Stream<Arguments> testUndoData() {
        return Stream.of(
                Arguments.of(0, 0, Direction.North, 0, 0,Direction.North),
                Arguments.of(1, 1, Direction.South, 1, 1,Direction.South),
                Arguments.of(0, 0, Direction.East, 0, 0,Direction.East),
                Arguments.of(0, 0, Direction.West, 0, 0,Direction.West)
        );
    }

    @ParameterizedTest()
    @MethodSource("testTurnLeftData")
    public void should_change_correct_xy_when_executeCommand_given_xy_and_turnLeft(int x,int y,Direction direction,
                                                                               int expectedX,int expectedY,Direction expectedDirection) {
        Location location = new Location(x,y,direction);
        MarsRover marsRover = new MarsRover(location);
        Command turnLeftCommand = new TurnLeftCommand(marsRover);
        turnLeftCommand.execute();

        assertEquals(expectedX, location.getX());
        assertEquals(expectedY, location.getY());
        assertEquals(expectedDirection, location.getDirection());
    }

    @ParameterizedTest()
    @MethodSource("testUndoData")
    public void should_constant_correct_xy_when_executeCommand_given_xy_and_turnLeft_and_undo(int x,int y,Direction direction,
                                                                               int expectedX,int expectedY,Direction expectedDirection) {
        Location location = new Location(x,y,direction);
        MarsRover marsRover = new MarsRover(location);
        Command turnLeftCommand = new TurnLeftCommand(marsRover);
        turnLeftCommand.execute();
        turnLeftCommand.undo();

        assertEquals(expectedX, location.getX());
        assertEquals(expectedY, location.getY());
        assertEquals(expectedDirection, location.getDirection());
    }
}
