package com.afs.tdd;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveCommandTest {

    public static Stream<Arguments> testMoveData() {
        return Stream.of(
                Arguments.of(0, 0, Direction.North, 0, 1,Direction.North),
                Arguments.of(0, 0, Direction.South, 0, -1,Direction.South),
                Arguments.of(0, 0, Direction.East, 1, 0,Direction.East),
                Arguments.of(0, 0, Direction.West, -1, 0,Direction.West),
                Arguments.of(1, 0, Direction.North, 1, 1,Direction.North),
                Arguments.of(0, 1, Direction.North, 0, 2,Direction.North)
        );
    }

    public static Stream<Arguments> testUndoData() {
        return Stream.of(
                Arguments.of(0, 0, Direction.North, 0, 0,Direction.North),
                Arguments.of(0, 0, Direction.South, 0, 0,Direction.South),
                Arguments.of(0, 0, Direction.East, 0, 0,Direction.East),
                Arguments.of(0, 0, Direction.West, 0, 0,Direction.West),
                Arguments.of(1, 0, Direction.North, 1, 0,Direction.North),
                Arguments.of(0, 1, Direction.North, 0, 1,Direction.North)
        );
    }

    @ParameterizedTest()
    @MethodSource("testMoveData")
    public void should_change_correct_xy_direction_when_executeCommand_given_xy_and_move(int x,int y,Direction direction,
                                                                               int expectedX,int expectedY,Direction expectedDirection) {
        Location location = new Location(x,y,direction);
        MarsRover marsRover = new MarsRover(location);
        MoveCommand moveCommand = new MoveCommand(marsRover);
        moveCommand.execute();

        assertEquals(expectedX, location.getX());
        assertEquals(expectedY, location.getY());
        assertEquals(expectedDirection, location.getDirection());
    }

    @ParameterizedTest()
    @MethodSource("testUndoData")
    public void should_constant_xy_direction_when_executeCommand_given_xy_and_move_and_undo(int x,int y,Direction direction,
                                                                               int expectedX,int expectedY,Direction expectedDirection) {
        Location location = new Location(x,y,direction);
        MarsRover marsRover = new MarsRover(location);
        MoveCommand moveCommand = new MoveCommand(marsRover);
        moveCommand.execute();
        moveCommand.undo();

        assertEquals(expectedX, location.getX());
        assertEquals(expectedY, location.getY());
        assertEquals(expectedDirection, location.getDirection());
    }
}
