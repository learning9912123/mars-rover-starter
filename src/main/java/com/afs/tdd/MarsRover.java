package com.afs.tdd;


public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void forward(){
        location.forward();
    }

    public void stepBack(){
        location.stepBack();
    }

    public void turnLeft(){
        location.turnLeft();
    }

    public void turnRight(){
        location.turnRight();
    }

}
