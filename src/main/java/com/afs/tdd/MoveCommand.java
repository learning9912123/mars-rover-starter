package com.afs.tdd;

public class MoveCommand extends Command {

    public MoveCommand(MarsRover marsRover) {
        super(marsRover);
    }

    @Override
    public boolean execute() {
        marsRover.forward();
        return true;
    }

    @Override
    boolean undo() {
        marsRover.stepBack();
        return true;
    }
}
