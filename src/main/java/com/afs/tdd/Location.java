package com.afs.tdd;

import java.util.HashMap;
import java.util.Map;

public class Location {
    private int x;
    private int y;
    private Direction direction;

    private final Map<Direction, Integer[]> moveMap = new HashMap<>();

    private final Direction[] directionArray = new Direction[4];

    private int directionIndex;

    public Location(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        initMoveMap();
        initDirectionArray();
    }

    private void initMoveMap() {
        moveMap.put(Direction.North, new Integer[]{0, 1});
        moveMap.put(Direction.South, new Integer[]{0, -1});
        moveMap.put(Direction.East, new Integer[]{1, 0});
        moveMap.put(Direction.West, new Integer[]{-1, 0});
    }

    private void initDirectionArray() {
        directionArray[0] = Direction.North;
        directionArray[1] = Direction.East;
        directionArray[2] = Direction.South;
        directionArray[3] = Direction.West;
        for (int i = 0; i < directionArray.length; i++) {
            if (this.direction == directionArray[i]) {
                directionIndex = i;
                break;
            }
        }
    }

    public void forward() {
        Integer[] dir = moveMap.get(direction);
        x += dir[0];
        y += dir[1];
    }

    public void stepBack() {
        Integer[] dir = moveMap.get(direction);
        x -= dir[0];
        y -= dir[1];
    }

    public void turnLeft() {
        directionIndex = (directionIndex + 3) % 4;
        direction = directionArray[directionIndex];
    }

    public void turnRight() {
        directionIndex = (directionIndex + 1) % 4;
        direction = directionArray[directionIndex];
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }


}
