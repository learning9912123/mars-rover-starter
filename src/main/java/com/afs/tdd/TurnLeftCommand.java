package com.afs.tdd;

public class TurnLeftCommand extends Command {

    public TurnLeftCommand(MarsRover marsRover) {
        super(marsRover);
    }

    @Override
    public boolean execute() {
        marsRover.turnLeft();
        return true;
    }

    @Override
    boolean undo() {
        marsRover.turnRight();
        return true;
    }
}
