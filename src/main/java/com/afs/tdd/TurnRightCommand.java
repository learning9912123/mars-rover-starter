package com.afs.tdd;

public class TurnRightCommand extends Command {

    public TurnRightCommand(MarsRover marsRover) {
        super(marsRover);
    }

    @Override
    public boolean execute() {
        marsRover.turnRight();
        return true;
    }

    @Override
    boolean undo() {
        marsRover.turnLeft();
        return true;
    }
}
