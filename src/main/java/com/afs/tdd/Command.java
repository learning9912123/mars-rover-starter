package com.afs.tdd;

public abstract class Command {
    protected MarsRover marsRover;

    public Command(MarsRover marsRover) {
        this.marsRover = marsRover;
    }

    abstract boolean execute();

    abstract boolean undo();


}
